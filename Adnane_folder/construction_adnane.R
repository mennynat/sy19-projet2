rm(list=ls())
library('kernlab')
library('MASS')
df.construction <- read.table("./data/construction_train.txt")


hist(df.construction[,c("y")])

n<-nrow(df.construction)
K_cv<-10
fold <- sample(K_cv, n, replace = TRUE)

chosen_predictors <- c("y", "X3", "X9", "X10", "X12", "X42", "X45", "X51", "X52", "X57",
                       "X59", "X60", "X62", "X64", "X65", "X69", "X76", "X88", "X92", "X93", 
                       "X97", "X99", "X100", "X103")  # best 

sigma_values <- c(seq(0.004,0.015,0.0005)) 
epsilon_values <- c(seq(0.03,0.08,0.005))
C_vals<-c(seq(7,15,1))
results_df <- data.frame(C = integer(0), Epsilon = numeric(0), Sigma = numeric(0), MSE = numeric(0))
for (c in C_vals){
  print("current C")
  print(c)
  for (epsi in epsilon_values){
    print("current epsi")
    print(epsi)
    for (sig in sigma_values){
      print("current sig")
      print(sig)
      for (k in 1:K_cv){
        model_svm<-ksvm(y~.,data=df.construction[fold!=k,chosen_predictors],kernel="rbfdot",type="eps-svr",C=c,epsilon=epsi,kpar=list(sigma=sig))
        pred_svm<-predict(model_svm,df.construction[fold==k,-104])
        mse_value<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
        results_df <- rbind(results_df, data.frame(C = c, Epsilon = epsi, Sigma = sig, MSE = mse_value))
      }
    }}}
library(dplyr)
results_df
mean_results_2 <- results_df %>%group_by(Sigma, Epsilon, C) %>%summarize(Mean_MSE = mean(MSE))


write.csv(results_df, file = "results_df_construction_smallerC.csv", row.names = FALSE)
df <- read.csv('results_df_construction_smallerC.csv')
mean_results_2 <- df %>%group_by(Sigma, Epsilon, C) %>%summarize(Mean_MSE = mean(MSE))
#######

chosen_predictors <- c("y", "X93", "X88", "X86", "X77", "X74", "X73", "X72", "X71", "X69", 
               "X68", "X66", "X65", "X64", "X63", "X60", "X59", "X58", "X57", "X56", 
               "X55", "X53", "X52", "X49", "X48", "X45", "X44", "X43", "X42", "X41", 
               "X40", "X39", "X37", "X36", "X35", "X34", "X33", "X32", "X31", "X30", 
               "X29", "X28", "X26", "X24", "X23", "X22", "X21", "X20", "X19", "X16", 
               "X14", "X12", "X9", "X6", "X5", "X4", "X3")
chosen_predictors <- c("X103", "y", "X102", "X101", "X97", "X96", "X95", "X93", 
               "X92", "X91", "X88", "X78", "X77", "X76", "X75", "X73", 
               "X71", "X62", "X61", "X60", "X58", "X57", "X56", "X54", 
               "X52", "X51", "X46", "X42", "X27", "X25", "X12", "X4")
chosen_predictors <- c("y", "X3", "X9", "X10", "X12", "X42", "X45", "X51", "X52", "X57",
                       "X59", "X60", "X62", "X64", "X65", "X69", "X76", "X88", "X92", "X93", 
                       "X97", "X99", "X100", "X103")  # best 
replayALOt<-function(){
n<-nrow(df.construction)
K_cv<-10
fold <- sample(K_cv, n, replace = TRUE)


mse_SVM <- rep(0, K_cv)
for (k in 1:K_cv){
  model_svm<-ksvm(y~.,data=df.construction[fold!=k,chosen_predictors],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
  pred_svm<-predict(model_svm,df.construction[fold==k,-104])
  mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
}
SVM_mse<-mean(mse_SVM)
#cat("Mean Squared Error using SVM-regression:", SVM_mse, "\n")
return(SVM_mse)
}
rep(replayALOt(),1)
######



############################################################################################
bad_predictors=c("X5", "X9", "X37", "X39", "X45", "X50", "X67", "X102")
inverse_predictors=c("X81", "X82", "X47", "X29")
questionable_predictors= c("X17", "X31", "X32", "X40", "X33", "X43", "X51", "X69", "X71", "X80", "X89", "X91", "X16")
further_variables <- c( "X8", "X18", "X42", "X48", "X52", "X54", "X61", "X64", "X65","X68", "X86", "X87", "X90", "X92", "X93", "X94", "X96", "X97", "X100", "X103")

used_predictors=c(questionable_predictors,inverse_predictors,bad_predictors,further_variables)
all_predictors <- paste0("X", 1:103)
good_predictors <- setdiff(all_predictors, used_predictors)


good_predictors<-c(good_predictors,"y")

df.construction <- read.table("./data/construction_train.txt")
#df.construction[,-104]=scale(df.construction[,-104])
library('kernlab')
library('MASS')
#further_variables <- c("X4", "X8", "X18", "X42", "X48", "X52", "X54", "X61", "X64", "X65", "X68", "X86", "X87", "X90", "X92", "X93", "X94", "X96", "X97", "X100", "X103")
used_predictors=c(questionable_predictors,inverse_predictors,bad_predictors)
all_predictors <- paste0("X", 1:103)
good_predictors <- setdiff(all_predictors, used_predictors)
good_predictors<-c(good_predictors,"y")
mse_SVM <- rep(0, K_cv)
for (k in 1:K_cv){
  model_svm<-ksvm(y~.,data=df.construction[fold!=k,c(good_predictors)],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
  pred_svm<-predict(model_svm,df.construction[fold==k,-104])
  mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
}
SVM_mse<-mean(mse_SVM)
cat("Mean Squared Error using SVM-regression:", SVM_mse, "\n")

#############################################################################
## initialization
mse_SVM <- rep(0, K_cv)
for (k in 1:K_cv){
  model_svm<-ksvm(y~.,data=df.construction[fold!=k,],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
  pred_svm<-predict(model_svm,df.construction[fold==k,-104])
  mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
}
last_mean_svm=mean(mse_SVM)


all_predictors <- paste0("X", 1:103)

current_predictors<-c(paste0("X", 1:103),"y")
for (i in all_predictors ){
  
  current_predictors <- setdiff(current_predictors, i)
  mse_SVM <- rep(0, K_cv)
  for (k in 1:K_cv){
    model_svm<-ksvm(y~.,data=df.construction[fold!=k,c(current_predictors)],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
    pred_svm<-predict(model_svm,df.construction[fold==k,-104])
    mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
  }
  if ( mean(mse_SVM) > last_mean_svm ){
    current_predictors <- c(current_predictors,i)
    print(i)
  }else {
  last_mean_svm=mean(mse_SVM)
  }
}

cat("Mean Squared Error using SVM-regression:", last_mean_svm, "\n")
#####################################################################################

####################### forward selection 


mse_SVM <- rep(0, K_cv)
for (k in 1:K_cv){
  model_svm<-ksvm(y~.,data=df.construction[fold!=k,c(1,104)],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
  pred_svm<-predict(model_svm,df.construction[fold==k,-104])
  mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
}
last_mean_svm=mean(mse_SVM)



all_predictors <- paste0("X", 1:103)
current_predictors<-c("X1","y")
for (i in all_predictors ){
  if (i=="X1"){
    print("we skipped")
    print(i)
    next
  }
  
  current_predictors <- c(current_predictors,i)
  
  mse_SVM <- rep(0, K_cv)
  for (k in 1:K_cv){
    model_svm<-ksvm(y~.,data=df.construction[fold!=k,c(current_predictors)],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
    pred_svm<-predict(model_svm,df.construction[fold==k,-104])
    mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
  }
  if ( mean(mse_SVM) > last_mean_svm ){
    current_predictors <- setdiff(current_predictors, i)
    print(i)
  }else {
    last_mean_svm=mean(mse_SVM)
  }
}
cat("Mean Squared Error using SVM-regression:", last_mean_svm, "\n")


current_predictors
mse_SVM <- rep(0, K_cv)
for (k in 1:K_cv){
  model_svm<-ksvm(y~.,data=df.construction[fold!=k,setdiff(current_predictors, "X1")],kernel="rbfdot",C=10,type="eps-svr",epsilon=0.01,kpar=list(sigma=0.01))
  pred_svm<-predict(model_svm,df.construction[fold==k,-104])
  mse_SVM[k]<-mean((pred_svm-df.construction[fold==k,c("y")])^2)
}
mean(mse_SVM)

###################################################################



########### 
#after getting the good results of 988 





##################################
new version squared : 
  
  to remove : 
X5 
X9

X37
X39


X45
X50
X67

  X102 
###
## to inverse 
  X81 inverse ?
    X82 inverse ?
    x47 inverse ?
    X29 inverse ?
####
to try : 
  X17 
  X31 
X32
X40
X33
X43

X51
X69 
X71 
  X80
  X89 
  X91 
  X16 
#################################
###################################


BEST  : 
  99 97 96 94   93  92 90  87  84 83  74 61 54 44 43    36 square    35 square  34 square 33 square 32
okay::
  X103 101 100  85  73 70 
  60 59 58 57 56 55  53  51 48
less than okay 
98  95   88   46
79  78  77 76 75 
68 66 63 62  40  38

bad   65 64   45 
91  89 86  82 81   80   67    50 49  47  41  39

I like : 
    101 100 99 97
  I dont like :
  102
  special :
    72  inverse 69
  inverse 52 
  42 inverse 
  37 sqaure
  