import sys

# Check if the correct number of arguments is provided
if len(sys.argv) != 2:
    print("Usage: python generate_images.py args_file")
    sys.exit(1)

# Read arguments from the file
args_file = sys.argv[1]
with open(args_file, 'r') as file:
    arguments = file.read().splitlines()

# Your code to process the arguments goes here
print("number of images :",len(arguments[0].split()))
#print(arguments[0].replace(",", "").split()[0])




import os
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array, array_to_img
import numpy as np
try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError:
    import subprocess
    import sys

    print("Matplotlib not found. Installing...")
    
    try:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'matplotlib'])
    except subprocess.CalledProcessError:
        print("Installation failed. Please install Matplotlib manually.")

    # Attempt to import again after installation
    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        print("Matplotlib installation unsuccessful. Please install Matplotlib manually.")
        sys.exit(1)

import tensorflow as tf
try:
    import cv2
except ModuleNotFoundError:
    import subprocess
    import sys

    print("OpenCV not found. Installing...")
    
    try:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'opencv-python'])
    except subprocess.CalledProcessError:
        print("Installation failed. Please install OpenCV manually.")

    # Attempt to import again after installation
    try:
        import cv2
    except ModuleNotFoundError:
        print("OpenCV installation unsuccessful. Please install OpenCV manually.")
        sys.exit(1)

###########
datagen = ImageDataGenerator(
    rescale=1./255,
    rotation_range=25,        # Random rotation up to 30 degrees
    zoom_range=0.1,           # Random zoom up to 20%
    horizontal_flip=True,
    vertical_flip=True,        # Enable vertical flipping
    width_shift_range=0.15,
    height_shift_range=0.15,
    fill_mode='nearest'        # Fill mode for newly created pixels
)

###########
counter=0
output_folder = './augmented_data'
images_paths= arguments[0].replace(",", "").split()
for img in images_paths :
    path_name = img.split('/')[-1].split('.')[0]
    if path_name.split('_')[0] == 'dog':
        output_folder='./Adnane_folder/augmented_data'+"/dog"
    elif path_name.split('_')[0] == 'car':
        output_folder='./Adnane_folder/augmented_data'+"/car"
    elif path_name.split('_')[0] == 'fruit':
        output_folder='./Adnane_folder/augmented_data'+"/fruit"
    else:
        print("Error : image name not valid")
        exit(1)

    img = cv2.imread(img)

    img= cv2.resize(img, (32,32))
    # output the original image
    cv2.imwrite(output_folder+"/"+path_name+".jpg",img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img.reshape((1,) + img.shape)
    i = 0
    # output the augmented image
    for batch in datagen.flow(img, batch_size=1, save_to_dir=output_folder, save_format='jpg', save_prefix=path_name):

        i += 1
        counter+=1
        if i > 0:  
            break

print("script finished successfully, ",counter," images generated")
        
