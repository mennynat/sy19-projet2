Modèle avec data augmentation !!!!!
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
################################################
### Images ###

rm(list=ls())
#install.packages("imager")
#install.packages("keras")
if (!requireNamespace("abind", quietly = TRUE)) {
  install.packages("abind")
}
library(abind)
library(imager)
library(keras)


## recupérer les PATHS de chaque image et label

# cars
image_car_folder<-"Data/images_train/car/"
image_car_list <- list.files(image_car_folder, full.names = TRUE)
label<-rep(0,length(image_car_list))
df_car_images <-data.frame(path=image_car_list,label=label)


# dogs
image_dog_folder<-"Data/images_train/dog/"
image_dog_list <- list.files(image_dog_folder, full.names = TRUE)
label<-rep(1,length(image_dog_list))
df_dog_images <-data.frame(path=image_dog_list,label=label)

# fruit

image_fruit_folder<-"Data/images_train/fruit/"
image_fruit_list <- list.files(image_fruit_folder, full.names = TRUE)
label<-rep(2,length(image_fruit_list))
df_fruit_images <-data.frame(path=image_fruit_list,label=label)


# all_images dataframe

df_images<-rbind(df_car_images,df_dog_images,df_fruit_images)

p_test<-.1
n<-nrow(df_images)
indices_test<-sample(n,p_test*n,replace=FALSE)
df_test<-df_images[indices_test,]
df_images<-df_images[-indices_test,]
df_images<-df_images[seq_len(nrow(df_images)),]


arg1 <- df_images$path
arg2 <- df_images$label
python_script <- "./generate_images.py"
args_file <- "args.txt"

arg1_str <- toString(arg1)
arg2_str <- toString(arg2)

writeLines(c(arg1_str, arg2_str), args_file)

command <- paste(shQuote("python"), shQuote(python_script), shQuote(args_file))


system(command)
#############################################
#### input the new augmented data


## recupérer les PATHS de chaque image et label

# cars
image_car_folder<-"augmented_data/car/"
image_car_list <- list.files(image_car_folder, full.names = TRUE)
label<-rep(0,length(image_car_list))
df_car_images <-data.frame(path=image_car_list,label=label)


# dogs
image_dog_folder<-"augmented_data/dog/"
image_dog_list <- list.files(image_dog_folder, full.names = TRUE)
label<-rep(1,length(image_dog_list))
df_dog_images <-data.frame(path=image_dog_list,label=label)

# fruit

image_fruit_folder<-"augmented_data/fruit/"
image_fruit_list <- list.files(image_fruit_folder, full.names = TRUE)
label<-rep(2,length(image_fruit_list))
df_fruit_images <-data.frame(path=image_fruit_list,label=label)
# group all new augmented dfs
df_images<-rbind(df_car_images,df_dog_images,df_fruit_images)
############################################



################### another way to store data in a df ######################
######################################################
common_size <- 100
preprocess_image <- function(image_path, image_label) {
  
  image_loaded <- load.image(image_path)
  
  if (dim(image_loaded)[1]!=common_size || dim(image_loaded)[2]!=common_size)
  { cat("PROBLEM SIZEEEEEEEEEEEEEEE")
    image_loaded=resize(image_loaded,size_x = common_size,size_y=common_size)
    }
  image_resized<-image_loaded[,,,1:3]
  
  result <- data.frame(image_resized = I(list(image_resized)), classes = image_label)  
  return(result)
}

df_single_image <- mapply(preprocess_image, df_images$path, df_images$label, SIMPLIFY = FALSE)

df_matrix <- as.data.frame(do.call(rbind, df_single_image))

#############################################

convertImage <-function(inputDFImage){
  Result<-lapply(inputDFImage,function(x) array(unlist(x),dim=(c(common_size,common_size,3))))
  Result<-abind(Result, along = 4)
  Result<-aperm(Result, c(4, 1, 2, 3))
}

#df_imagesONLY<-convertImage(df_matrix$image_resized)

#plot(permute_axes(as.cimg(df_converted[1,,,]),"yxzc")) 



training_index<-sample(length(df_matrix$classes),(8/10) * length(df_matrix$classes) ,replace = FALSE)
train_data <- df_matrix[training_index, ]
val_data<-df_matrix[-training_index, ]
#remaining_indices<-setdiff(1:length(df_matrix$classes),training_index)
#val_indices<-sample(remaining_indices,0.6*length(remaining_indices),replace=FALSE)
#val_indices<-setdiff(remaining_indices,val_indices)

#val_data<-df_matrix[val_indices, ]
#test_data <- df_matrix[-c(training_index,val_indices), ]
### sans ensemble de test // val_data <- df_matrix[-training_index, ]

x_train <- convertImage(train_data$image_resized)
x_val <- convertImage(val_data$image_resized)
#x_test <- convertImage(test_data$image_resized)

plot(permute_axes(as.cimg(x_val[1280,,,]),"yxzc")) 




############# tests sur une image #########################
#common_size<-100
#image_loaded_test<-load.image(df_images$path[1])
#original_test<--load.image(df_images$path[1])
#original_test<--resize(original_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-resize(original_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-image_loaded_test[,,,1:3]
#plot(permute_axes(as.cimg(image_loaded_test),"yxzc")) 

#image_loaded_test<-resize(image_loaded_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-as.matrix(image_loaded_test)
#image_loaded_test
#attributes(image_loaded_test)
#as.data.frame(image_loaded_test)
#image_to_array(image_loaded_test)






#### Model
batch_size<-30
model <- keras_model_sequential()

# Add a convolutional layer with 32 filters, 3x3 kernel size, and 'relu' activation
model %>%
  layer_conv_2d(filters = 32, kernel_size = c(3, 3), input_shape = c(common_size, common_size, 3), activation = 'relu') %>%
  
  # Add a max pooling layer
  layer_max_pooling_2d(pool_size = c(2, 2)) %>%
  
  # Add another convolutional layer with 64 filters, 3x3 kernel size, and 'relu' activation
  layer_conv_2d(filters = 64, kernel_size = c(3, 3), activation = 'relu') %>%
  
  # Add another max pooling layer
  layer_max_pooling_2d(pool_size = c(2, 2)) %>%
  
  # Flatten the output for the fully connected layers
  layer_flatten() %>% 
  
  # Add a dense (fully connected) layer with 128 neurons and 'relu' activation
  layer_dense(units = 128, activation = 'relu') %>%
  
  # Add the output layer with the number of classes and 'softmax' activation
  layer_dense(units = 3, activation = 'softmax')

# Compile the model with categorical crossentropy loss and Adam optimizer
model %>% compile(
  loss = 'sparse_categorical_crossentropy',
  optimizer = optimizer_adam(),
  metrics = c('accuracy')
)

# Display the model summary
summary(model)

# Fit data into model
history <- model %>% 
  fit(
    x=x_train,
    y=as.integer(train_data$classes),
    # training epochs
    steps_per_epoch = ceiling(nrow(train_data)/batch_size), 
    epochs = 10, 
    
    # validation data
    validation_data = list(x_val, as.integer(val_data$classes)),
    validation_steps = ceiling(nrow(val_data) / batch_size)
  )


## Evaluer le modèle 

train_metrics <- model %>% evaluate(x_train, as.numeric(train_data$classes))
cat("Training Metrics:\n")
print(train_metrics)

# Evaluate the model on the validation data
val_metrics <- model %>% evaluate(x_val, as.numeric(val_data$classes))
cat("Validation Metrics:\n")
print(val_metrics)


## Predictions



df_test_mapply <- mapply(preprocess_image, df_test$path, df_test$label, SIMPLIFY = FALSE)

df_test_2 <- as.data.frame(do.call(rbind, df_test_mapply))


x_test<-convertImage(df_test_2$image_resized)
predictions <- model %>% predict(x_test)


predicted_classes <- max.col(predictions) - 1
confusion_matrix <- table(predicted_classes, as.integer(df_test_2$classes))
confusion_matrix
accuracy_test<-sum(diag(confusion_matrix))/sum(confusion_matrix)
cat("la précision du modèle dans les données de test : ",accuracy_test)

## save & load 
model %>% save_model_tf("model_CNN/my_model.keras")
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



MODEL CLASSIC 



################################################
### Images ###

rm(list=ls())
#install.packages("imager")
#install.packages("keras")
if (!requireNamespace("abind", quietly = TRUE)) {
  install.packages("abind")
}
library(abind)
library(imager)
library(keras)


## recupérer les PATHS de chaque image et label

# cars
image_car_folder<-"Data/images_train/car/"
image_car_list <- list.files(image_car_folder, full.names = TRUE)
label<-rep(0,length(image_car_list))
df_car_images <-data.frame(path=image_car_list,label=label)


# dogs
image_dog_folder<-"Data/images_train/dog/"
image_dog_list <- list.files(image_dog_folder, full.names = TRUE)
label<-rep(1,length(image_dog_list))
df_dog_images <-data.frame(path=image_dog_list,label=label)

# fruit

image_fruit_folder<-"Data/images_train/fruit/"
image_fruit_list <- list.files(image_fruit_folder, full.names = TRUE)
label<-rep(2,length(image_fruit_list))
df_fruit_images <-data.frame(path=image_fruit_list,label=label)


# all_images dataframe

df_images<-rbind(df_car_images,df_dog_images,df_fruit_images)

p_test<-.1
n<-nrow(df_images)
indices_test<-sample(n,p_test*n,replace=FALSE)
df_test<-df_images[indices_test,]
df_images<-df_images[-indices_test,]
df_images<-df_images[seq_len(nrow(df_images)),]



################### another way to store data in a df ######################
######################################################
common_size <- 100
preprocess_image <- function(image_path, image_label) {
  
  image_loaded <- load.image(image_path)
  
  if (dim(image_loaded)[1]!=common_size || dim(image_loaded)[2]!=common_size)
  { cat("PROBLEM SIZEEEEEEEEEEEEEEE")
    image_loaded=resize(image_loaded,size_x = common_size,size_y=common_size)
  }
  image_resized<-image_loaded[,,,1:3]
  
  result <- data.frame(image_resized = I(list(image_resized)), classes = image_label)  
  return(result)
}

df_single_image <- mapply(preprocess_image, df_images$path, df_images$label, SIMPLIFY = FALSE)

df_matrix <- as.data.frame(do.call(rbind, df_single_image))

#############################################

convertImage <-function(inputDFImage){
  Result<-lapply(inputDFImage,function(x) array(unlist(x),dim=(c(common_size,common_size,3))))
  Result<-abind(Result, along = 4)
  Result<-aperm(Result, c(4, 1, 2, 3))
}

#df_imagesONLY<-convertImage(df_matrix$image_resized)

#plot(permute_axes(as.cimg(df_converted[1,,,]),"yxzc")) 



training_index<-sample(length(df_matrix$classes),(8/10) * length(df_matrix$classes) ,replace = FALSE)
train_data <- df_matrix[training_index, ]
val_data<-df_matrix[-training_index, ]
#remaining_indices<-setdiff(1:length(df_matrix$classes),training_index)
#val_indices<-sample(remaining_indices,0.6*length(remaining_indices),replace=FALSE)
#val_indices<-setdiff(remaining_indices,val_indices)

#val_data<-df_matrix[val_indices, ]
#test_data <- df_matrix[-c(training_index,val_indices), ]
### sans ensemble de test // val_data <- df_matrix[-training_index, ]

x_train <- convertImage(train_data$image_resized)
x_val <- convertImage(val_data$image_resized)
#x_test <- convertImage(test_data$image_resized)

plot(permute_axes(as.cimg(x_val[10,,,]),"yxzc")) 




############# tests sur une image #########################
#common_size<-100
#image_loaded_test<-load.image(df_images$path[1])
#original_test<--load.image(df_images$path[1])
#original_test<--resize(original_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-resize(original_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-image_loaded_test[,,,1:3]
#plot(permute_axes(as.cimg(image_loaded_test),"yxzc")) 

#image_loaded_test<-resize(image_loaded_test,size_x=common_size,size_y=common_size)
#image_loaded_test<-as.matrix(image_loaded_test)
#image_loaded_test
#attributes(image_loaded_test)
#as.data.frame(image_loaded_test)
#image_to_array(image_loaded_test)






#### Model
batch_size<-30
model <- keras_model_sequential()

# Add a convolutional layer with 32 filters, 3x3 kernel size, and 'relu' activation
model %>%
  layer_conv_2d(filters = 32, kernel_size = c(3, 3), input_shape = c(common_size, common_size, 3), activation = 'relu') %>%
  
  # Add a max pooling layer
  layer_max_pooling_2d(pool_size = c(2, 2)) %>%
  
  # Add another convolutional layer with 64 filters, 3x3 kernel size, and 'relu' activation
  layer_conv_2d(filters = 64, kernel_size = c(3, 3), activation = 'relu') %>%
  
  # Add another max pooling layer
  layer_max_pooling_2d(pool_size = c(2, 2)) %>%
  
  # Flatten the output for the fully connected layers
  layer_flatten() %>% 
  
  # Add a dense (fully connected) layer with 128 neurons and 'relu' activation
  layer_dense(units = 128, activation = 'relu') %>%
  
  # Add the output layer with the number of classes and 'softmax' activation
  layer_dense(units = 3, activation = 'softmax')

# Compile the model with categorical crossentropy loss and Adam optimizer
model %>% compile(
  loss = 'sparse_categorical_crossentropy',
  optimizer = optimizer_adam(),
  metrics = c('accuracy')
)

# Display the model summary
summary(model)

# Fit data into model
history <- model %>% 
  fit(
    x=x_train,
    y=as.integer(train_data$classes),
    # training epochs
    steps_per_epoch = ceiling(nrow(train_data)/batch_size), 
    epochs = 10, 
    
    # validation data
    validation_data = list(x_val, as.integer(val_data$classes)),
    validation_steps = ceiling(nrow(val_data) / batch_size)
  )


## Evaluer le modèle 

train_metrics <- model %>% evaluate(x_train, as.numeric(train_data$classes))
cat("Training Metrics:\n")
print(train_metrics)

# Evaluate the model on the validation data
val_metrics <- model %>% evaluate(x_val, as.numeric(val_data$classes))
cat("Validation Metrics:\n")
print(val_metrics)


## Predictions



df_test_mapply <- mapply(preprocess_image, df_test$path, df_test$label, SIMPLIFY = FALSE)

df_test_2 <- as.data.frame(do.call(rbind, df_test_mapply))


x_test<-convertImage(df_test_2$image_resized)
predictions <- model %>% predict(x_test)


predicted_classes <- max.col(predictions) - 1
confusion_matrix <- table(predicted_classes, as.integer(df_test_2$classes))
confusion_matrix
accuracy_test<-sum(diag(confusion_matrix))/sum(confusion_matrix)
cat("la précision du modèle dans les données de test : ",accuracy_test)

## save & load 
model %>% save_model_tf("model_CNN/model_classic.keras")


# https://tensorflow.rstudio.com/install/