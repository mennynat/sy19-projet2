library(mixtools)
library(mclust)
library('kernlab')
library('MASS')
library(gam)
data.robotics<-read.csv("Data/robotics_train.txt",header=TRUE,sep = " ")

n<-nrow(data.robotics)
K_cv<-10
fold <- sample(K_cv, n, replace = TRUE)

mse_svm<-rep(0,K_cv)
for (k in 1:K_cv)
{
  model_svm<-ksvm(y~.,data=data.robotics[fold!=k,],scaled=TRUE,kernel="rbfdot",type="eps-svr",C=10,epsilon=0.12,kpar=list(sigma=0.15))
  pred_svm<-predict(model_svm,data.robotics[fold==k,-9])
  mse_svm[k]=mean((pred_svm-data.robotics[fold==k,9])^2)
}
mean_mse_svm<-mean(mse_svm)
cat("mean svm is :",mean_mse_svm)
Mixture.fit<-regmixEM(y=data.robotics[fold!=k,9],x=as.matrix(data.robotics[fold!=k,-9]))
summary(Mixture.fit)
predict(Mixture.fit,newdata=data.robotics[fold==k,-9])


regmixEM(data.robotics[,])
?regmixEM
?Mclust


