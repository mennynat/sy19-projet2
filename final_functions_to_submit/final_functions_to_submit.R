library(kernlab)
library(MASS)


library(kernlab)
library(MASS)

library(FNN)

library(e1071)

# -------
# Phonemes

data.phonemes <- read.table("Data/phonemes_train.txt")

generateRnd <- function(n, df) {
  res <- data.frame(matrix(nrow=0, ncol=ncol(df)))
  colnames(res) <- names(df)
  for(i in 1:n) {
    newRow <- c()
    for(colName in names(df)) {
      if(colName == "y") {
        newRow <- c(newRow, "?")
      } else {
        newRow <- c(newRow, rnorm(1, 0, 1))
      }
    }
    summary(newRow)
    res[i,] <- newRow
  }
  
  res[,-257] <- sapply(res[,-257], as.numeric)
  return(res)
}

newD <- generateRnd(round(0.4 * nrow(data.phonemes)), data.phonemes)

data.phonemes <- rbind(data.phonemes, newD)

model.phoneme <- svm(as.factor(y) ~ ., data=data.phonemes)

# -------
# Robotics

data.robotics<-read.csv("Data/robotics_train.txt",header=TRUE,sep = " ")
#model.robotics<-ksvm(y~.,data=data.robotics,kernel="rbfdot",type="eps-svr",C=6,epsilon=0.12,kpar=list(sigma=0.16))
model.robotics<-ksvm(y~.,data=data.robotics,kernel="rbfdot",type="eps-svr",C=5.4,epsilon=0.10,kpar=list(sigma=0.16))
# -------
# Construction

data.construction <- read.table("Data/construction_train.txt")
chosen_predictors <- c("y", "X3", "X9", "X10", "X12", "X42", "X45", "X51", "X52", "X57",
                       "X59", "X60", "X62", "X64", "X65", "X69", "X76", "X88", "X92", "X93", 
                       "X97", "X99", "X100", "X103")
#model.construction<-ksvm(y~.,data=data.construction[,chosen_predictors],kernel="rbfdot",type="eps-svr",C=100,epsilon=0.005,kpar=list(sigma=0.0037))
model.construction<-ksvm(y~.,data=data.construction[,chosen_predictors],kernel="rbfdot",type="eps-svr",C=11,epsilon=0.055,kpar=list(sigma=0.0065))

# -----------
# Predictions

prediction_phoneme<-function(dataset){
  library(e1071)
  # Modèle de discrimination entre valeurs connues
  predict(model.phoneme, newdata=dataset)
}

prediction_robotics<-function(dataset){
  library(kernlab)
  library(MASS)
  predict(model.robotics,dataset)
}

prediction_construction<-function(dataset){
  library(kernlab)
  library(MASS)
  predict(model.construction, newdata=dataset)
}

save(
  "model.phoneme",
  "model.robotics",
  "model.construction",
  "prediction_phoneme",
  "prediction_robotics",
  "prediction_construction",
  file = "env.Rdata"
)
